# JS Menu syntax 

Syntactic sugar for creating telegram bot menu in JavaScript for [LibTelegram](https://gitlab.com/libtelegram)

## Simple example
```js
const child2 =
  @i18n
  'Button1' | 'Button2' | 'Button3'
              'Button4'
        'Button5' | 'Button6'
 
const child =
  @Text('Send text with menu')
  @Photo(photo_id)
  'Button label'@{child2} | 'Nothing to show'

const menu = 
  'Button label'@{child}
```

It is called JSM, and it is a syntax extension to JavaScript. We recommend using it with LibTelegram to describe what the Menu should look

*See more examples [here](https://gitlab.com/libtelegram/jsm-syntax/-/blob/master/examples/).*

## Why JSM?

LibTelegram embraces the fact that rendering logic is inherently coupled with other menu logic: how events are handled, how the state changes over time, and how the data is prepared for display.

LibTelegram doesn’t require using JSM, but most people find it helpful as a visual aid when working with Menu inside the JavaScript code. It also allows LibTelegram to show more useful error and warning messages.

With that out of the way, let’s get started!

## Embedding Expressions in JSM

```js
const menu = 
  `Hello, ${usr.name}`;

usr.reply('It is menu!', { menu });
```

## JSM is an Expression Too

After compilation, JSM expressions become regular JavaScript function calls and evaluate to JavaScript objects.

This means that you can use JSM inside of if statements and for loops, assign it to variables, accept it as arguments, and return it from functions:

```js
function getGreetingMenu(usr) {
  if (usr) {
    return (`Hello, ${usr.name}` | 'Bye!');  
  }

  return ('Hello, Stranger' | 'Bye!')
}
```

## Mapping an Array to Buttons with @for
We can use the `@for` directive to render a list of items based on an array. The `@for` directive requires a special syntax in the form of `item in items`, where `items` is the source data array and `item` is an alias for the array element being iterated on:

```js
const items = [
    { title: 'Foo' },
    { title: 'Bar' }
  ]

const menu = 
  [ item.title ]@for(item in items)

```

## Custom wrappers

Buttons can use wrappers

These three examples are identical:
```js
const addSym = (usr, text) => text + '1' 

const menu = 
  #addSym
  'button' | 'btn'
```

```js
const addSym = (usr, text) => text + '1' 

const menu = 
  addSym('button') | addSym('btn')
```

```js
const menu = 
  'button1' | 'btn1'
```

## Specifying Attributes with JSM

You may use js expression to specify attributes:

```js
const photo_id = 1234567890;

const menu = 
  @Photo(photo_id)
  'I like it!'
```

## Specifying Children with JSM

Menu may contain submenu:

```js
const child = 
  'I am the child!'

const menu = 
  'Show me child'@{child}
```


## JSM Represents Objects 

LibTelegram compiles JSM down to LibT.createElement() calls.

These two examples are identical:

```js
const menu =
  @Text('Send text')
  'Hello!'@{action}
```

```js
const menu = LibTelegram.createElement(
  'menu',
  {
    text: 'send text'
  },
  [
    [
      LibTelegram.createElement(
        'button',
        {
          title: 'Hello!'
        },
        action
      )
    ]
  ]
);
```

These objects are called "LibTelegram elements”. You can think of them as descriptions of what you want to see on the screen. LibTelegram reads these objects and uses them to construct the menu and keep it up to date.