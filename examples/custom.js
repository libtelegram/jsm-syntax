const { i18n } = require('@/utils/');

const menu = 
	#i18n
	'btn1' | 'btn2'

/**
 * Similiar to
 * const menu = 
 *   i18n('btn1) | i18n('btn2') 
 */

module.exports = menu;