const child2 =
	@Text('Send text')
	@resized
  'Button1' | 'Button2' | 'Button3'
              'Button4'
        'Button5' | 'Button6'
 
const child =
  @Text('Send text with menu')
  @Photo(photo_id)
  'Button label'@{child2} | 'Nothing to show'

const menu = 
	'Button label'@{child}

module.exports = menu;