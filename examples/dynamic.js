const PaymentHadler = require('..../handlers/payment.js');

const payment_methods = [
	{ title: 'PayPal', id: 'paypal' },
	{ title: 'Yandex.money', id: 'yandex' },
	{ title: 'Stripe', id: 'stripe' }
]

// dynamic menu
const payment =
	@callback('p', PaymentHadler)
	@Text('choice payment method')
	[
		method.title@{ method.id } 
	]@for(method in payment_methods)
	'Back'@{back}

/** Similiar to 
const payment = 
	@callback('p', PaymentHadler)
	@Text('choice payment method')
	'PayPal'@{'paypal'}
	'Yandex.money'@{'yandex'}
	'Stripe'@{'stripe'},
	'Back'@{back}
*/

const menu = // reply menu
	'Buy it!'@{payment}

module.exports = menu;