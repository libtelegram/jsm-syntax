const child2 = 
	@Text('I\'m child!') // Send text with menu
	@callback('child2')
	'back'@{'back', back} // callback_data = 'child2' + separator + 'back'; handler = back 

const child =
	@callback('child')
	@Text('do you want to see my child?')
	'yes, I do'@{'child2', child2} | 'No, I don\'t'@{'back', back}

const menu =
	@callback('m')
	'Hello!'@{'child', child}

/**
 * Using:
 * const menu = require('.../callback');
 * 
 * usr.reply('hello', { menu })
 */

module.exports = menu;